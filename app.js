var express = require("express");
var app=express();

//Load express

var bodyParser=require("body-parser");
// app.use(bodyParser.urlencoded({'extended': 'true'}));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// var queue = require("q");
var q = require("q");

var mysql = require("mysql");
var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "root",
    password: "fsfv2",
    database: "sakila",
    connectionLimit: 4
});

// const findAllStmt = "select film_id, title, release_year from film limit ? offset ?";
const findAllStmt = "select film_id, title, release_year from film";
const findOneStmt = "select * from film where film_id = ?";

var makeQuery = function (sql, pool) {
    return function (args) {

        var defer = q.defer();
        pool.getConnection(function (err, connection) {
            if (err) {
                return defer.reject(err);
            }
            connection.query(sql, args || [], function (err, result) {
                connection.release();
                if (err) {
                    return defer.reject(err);
                }
                defer.resolve(result);
            });
        });
        return defer.promise;
    }
};

// var makeQuery = function (sql, pool) {
//     return (function (args) {
//         var defer = q.defer();
//         pool.getConnection(function (err, conn) {
//             if (err) {
//                 defer.reject(err);
//                 return;
//             }
//             conn.query(sql, args || [], function (err, results) {
//                 conn.release();
//                 if (err) {
//                     defer.reject(err);
//                     return;
//                 }
//                 defer.resolve(results);
//             });
//         });
//         return (defer.promise);
//     });
// };

var findAll = makeQuery(findAllStmt, pool);
var findOne = makeQuery(findOneStmt, pool);

app.get("/api/films", function (req, res) {
    findAll()
        .then(function (films) {
            res.status(200).json(films);
        })
        .catch(function (err) {
            console.info(err);
            res.status(500).end();
        })
});

app.get("/api/film/:filmId", function (req, res) {
    // console.info("App Detail: " + filmId);
    console.info("App Detail: " + req.params.filmId);
    findOne(req.params.filmId)
        .then(function (film) {
            res.status(200).json(film[0]);
        })
        .catch(function (err) {
            console.info(err);
            res.status(500).end();
        })
});

// app.get("/api/films", function (req, res) {
//     res.status(200).json([{
//         film_id: 1,
//         name: "Avengers"
//     }, {
//         film_id: 2,
//         name: "Avengers 2"
//     }])
// });
//
// app.get("/api/films/:fid", function (req, res) {
//     res.status(200).json({
//         film_id: 1,
//         name: "Avengers1",
//         description: "abcde abcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcde"
//     })
// });

// Serve public files
app.use(express.static(__dirname + "/public"));
// Handle Errors
app.use(function (req, res, http) {
    res.redirect("error.html");
});

// Set port
app.set("port",
    process.argv[2] || process.env.APP_PORT || 3017
);

// Start the Express server
app.listen(app.get("port"), function () {
    console.info("Express server started on port %s", app.get("port"));
});
