/**
 * Created by pohchye on 21/7/2016.
 */

(function () {
    
    angular
        .module("DemoApp", ["ui.router"])
        .config(DemonConfig)
        .controller("DogsCtrl", DogsCtrl);

    DogsCtrl.$inject = ["$state", "$stateParams"];

    function DogsCtrl($state, $stateParams) {
        console.log("Hello from Dogs controller");
        console.log("Parameters: " + $stateParams);
        var vm = this;
        vm.message = "Hello Using Controller";

        vm.bark = function () {
            // alert("Bow Bow!");
            // $state.go("cats");
            alert($stateParams.dogName + " is barking");
        };

        // $http.get("/api/dogs" + $stateParams.dogname);
    }
    
    DemonConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
    
    function DemonConfig($stateProvider, $urlRouteProvider) {
        $stateProvider
            .state("home", {
                url: "/",
                // template: "<h1>Hello World</h1>"
                templateUrl: "views/home.html",
                controller: "DogsCtrl as ctrl"
            })
            .state("cats", {
                // url: "/cats",
                url: "/cats-home-page",
                // template: "<h1>Hello Cats</h1>"
                templateUrl: "views/cats.html",
                controller: "DogsCtrl as ctrl"
            })
            .state("dogs", {
                // url: "/dogs",
                url: "/dogs/:dogName",
                // template: "<h1>Hello Dogs</h1>"
                templateUrl: "views/dogs.html",
                controller: "DogsCtrl as dogs"
            });
        
        $urlRouteProvider.otherwise("/");
    }
    
})();
