(function () {
    angular.module("FilmApp")
        .controller("ListCtrl", ListCtrl)
        .controller("DetailCtrl", DetailCtrl);

    function ListCtrl(dbService) {
        var vm = this;

        dbService.list()
            .then(function (films) {
                vm.films = films;
            })
            .catch(function (err) {
                console.log("Somer Error Occured", err);
            });
    }

    ListCtrl.$inject = ["dbService"];

    function DetailCtrl($stateParams, dbService) {
        var vm = this;
        console.log("DetailCtrl filmId: " + $stateParams.filmId);
        dbService.detail($stateParams.filmId)
            .then(function (film) {
                console.log("DetailCtrl result: ", film);
                vm.film = film;
            })
    }

    DetailCtrl.$inject = ["$stateParams", "dbService"];
}());